.. myFirstPackage_TrialGuyOne documentation master file, created by
   sphinx-quickstart on Sat Apr 28 22:08:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to myFirstPackage_TrialGuyOne's documentation!
======================================================

This is a simple Python Package for studying and learning python.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial
   project
   code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
