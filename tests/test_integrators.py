import myFirstPackage as pac

def test_run():
    obj=pac.try_01(pac.ToAdd,5,6)
    assert obj.run()==11
    obj=pac.try_01(pac.ToSub,5,6)
    assert obj.run()==-1
    obj=pac.try_01(pac.ToMultiply,5,6)
    assert obj.run()==30